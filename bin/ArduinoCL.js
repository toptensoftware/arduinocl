#!/usr/bin/env node

var child_process = require('child_process');
var path = require('path');
var fs = require('fs');
var os = require('os');
var Q = require('q');
var requestify = require('requestify');
var semverCompare = require('semver-compare');
var download = require('download');
var extractZip = require('extract-zip');
var expandHomeDir = require('expand-home-dir');

// Work out current folder
var cwd = process.cwd();
var folderName = path.parse(cwd).name;

// Start with default arduino installation location
var options = {}

// Setup platform specific defaults
switch (os.platform())
{
    case "win32":
        options.arduinoDir = getEnv("ARDUINO_DIR", "C:\\Program Files (x86)\\Arduino");
        options.userFolder = process.env.USERPROFILE;
        options.userLibraries = path.join(process.env.USERPROFILE, 'Documents/Arduino/libraries');
        break;

    default:
        options.arduinoDir = getEnv("ARDUINO_DIR",	"/Applications/Arduino.app/Contents/Java");
        options.userFolder = expandHomeDir("~");
        options.userLibraries = expandHomeDir("~/Documents/Arduino/libraries");
        break;
}

// Merge option files
merge(options, parseOptions(path.join(options.userFolder, ".ArduinoCL/acl.json")));
merge(options, parseOptions("acl.json"));
merge(options, parseOptions("acl.user.json"));

// Resolve SDK locations
mergeMissing(options, {
    arduinoBuilder: path.join(options.arduinoDir, "arduino-builder"),
    boardsTxt: path.join(options.arduinoDir, "hardware/arduino/avr/boards.txt"),
    arduinoHardware: path.join(options.arduinoDir, "hardware"),
    arduinoTools: path.join(options.arduinoDir, "hardware/tools"),
    arduinoToolsBuilder: path.join(options.arduinoDir, "tools-builder"),
    arduinoBuiltInLibraries: path.join(options.arduinoDir, "libraries"),
    avrDude: path.join(options.arduinoDir, "hardware/tools/avr/bin/avrdude"),
    avrDudeConf: path.join(options.arduinoDir, "hardware/tools/avr/etc/avrdude.conf"),
    libraries: [],
    library_index: path.join(os.tmpdir(), 'ArduinoCL/library_index.json'),
    library_index_url: "http://downloads.arduino.cc/libraries/library_index.json",
})


// Port environment setting overrides any option settings (but not command line)
if (process.env.ARDUINO_PORT)
{
    options.port = process.env.ARDUINO_PORT;
}



// ------------ Command Line Parse --------------

if (process.argv[2] == 'help' || process.argv[2] == '-h' || process.argv[2] == '--help' || process.argv[2] == '/?')
{
    showHelp();
}
else if (process.argv[2] == 'boards')
{
    listBoards();
}
else if (process.argv[2] == 'lib')
{
    // Library tools
    if (process.argv.length > 3)
    {
        Q.async(function*() {
            switch (process.argv[3])
            {
                case 'refresh':
                    yield* loadLibIndex(true);
                    break;

                case 'list':
                    yield* listLibraries(process.argv[4]);
                    break;

                case 'info':
                    yield* showLibraryInfo(process.argv[4], process.argv[5]);
                    break;

                case 'install':
                    yield* installLibrary(process.argv[4], process.argv[5]);
                    break;
            }
        })();
    }
}
else
{
    var compile = true;
    var upload = false;
    var clean = false;

    // Build tools
    for (var i=2; i<process.argv.length; i++)
    {
        switch (process.argv[i])
        {
            case 'compile':
                compile = true;
                upload = false;
                break;

            case 'upload':
                compile = true;
                upload = true;
                break;

            case 'clean':
                clean = true;
                compile = false;
                break;

            case 'rebuild':
                clean = true;
                compile = true;
                break;

            case '-v':
            case '--verbose':
                options.verbose = true;
                break;

            case '-P':
            case '--port':
                options.port = process.argv[i+1];
                i++;
                break;

            case '--board':
                options.board = process.argv[i+1];
                i++;
                break;

            case '--processor':
                options.processor = process.argv[i+1];
                i++;
                break;
        }
    }

    doBuild(clean, compile, upload);
}

// ------------ Do Build --------------

function doBuild(clean, compile, upload)
{
    // Parse boards.txt file
    var boardDefinitions = parseBoardDefinitions();

    // Resolve board type
    if (!options.board)
    {
        options.board = "uno";
    }

    // Check processor is supported
    if (options.processor)
    {
        // Force to lower case
        options.processor = options.processor.toLowerCase();

        // Handle variants
        if (options.processor == "atmega328p")
            options.processor = "atmega328";

        // Check processor is supported
        var bi = boardDefinitions.$boardInfos[options.board];
        if (!bi.processors)
        {
            console.log(`Ignoring processor option '${options.processor}' for board '${options.board}'`);
        }
        else if (!bi.processors[options.processor])
        {
            console.error(`Unsupported processor '${options.processor}' on board '${options.board}'`);
            process.exit(7);
        }
    }

    // Pick the default processor if not specified
    if (!options.processor && boardDefinitions.$defaultProcessors[options.board])
    {
        options.processor = boardDefinitions.$defaultProcessors[options.board];
    }

    function getBoardOption(name)
    {
        // Try with board processor qualification
        if (options.processor)
        {
            var key = options.board + ".menu.cpu." + options.processor + "." + name;
            if (boardDefinitions[key])
                return boardDefinitions[key];
        }

        // Try without processor qualification
        var key = options.board + "." + name;
        if (boardDefinitions[key])
            return boardDefinitions[key];

        // Unknown option
        console.error("Can't find board option:", key);
        process.exit(7);
    }

    // Resolve other defaults
    mergeMissing(options, {
        fqbn: "arduino:avr:" + options.board,
        part: getBoardOption("build.mcu"),
        baud: getBoardOption("upload.speed"),
        protocol: getBoardOption("upload.protocol"),
    });


    // Update fqbn to include processor
    var key = options.board + ".menu.cpu." + options.processor;
    if (boardDefinitions[key])
        options.fqbn += ":cpu=" + options.processor;

    // Work out the root ino file
    if (!options.rootIno)
    {
        // If specified, use it
        if (options.projectName)
        {
            options.rootIno = path.join(cwd, options.projectName + ".ino");
        }
        else
        {
            // Else look for an ino file with the same name as the folder
            var rootIno = path.join(cwd, folderName + ".ino");
            if (fs.existsSync(rootIno))
            {
                options.rootIno = rootIno;
                options.projectName = folderName;
            }
            else
            {
                if (options.verbose)
                {
                    console.log("Resolved options...");
                    console.log(options);
                    console.log();
                }
                console.error("Default sketch file ", folderName+".ino", "not found");
                process.exit(7);
            }
        }
    }

    if (!options.outputFolder)
    {
        options.outputFolder = path.join(os.tmpdir(), 'ArduinoCL/build/' + options.projectName);
    }

    if (options.verbose)
    {
        console.log("Resolved options...");
        console.log(options);
        console.log();
    }


    // ------------ Clean phase --------------

    if (clean)
    {
        console.log("Cleaning...");
        rmdir(options.outputFolder);
    }



    // ------------ Compile phase --------------

    if (compile)
    {
        // Make sure the output folder exists
        mkdirp(options.outputFolder);

        var buildArgs = [
            '-build-path', path.resolve(cwd, options.outputFolder),
            '-compile',
            '-hardware', options.arduinoHardware,
            '-fqbn', options.fqbn,
            '-build-cache', os.tmpdir(),
            '-verbose=' + !!options.verbose,
        ];
        pushOneOrArray(buildArgs, '-tools', options.arduinoTools);
        pushOneOrArray(buildArgs, '-tools', options.arduinoToolsBuilder);
        pushOneOrArray(buildArgs, '-built-in-libraries', options.arduinoBuiltInLibraries);
        pushOneOrArray(buildArgs, '-libraries', options.userLibraries);
        pushOneOrArray(buildArgs, '-libraries', options.libraries);
        buildArgs.push(options.rootIno);


        console.log(`Compiling sketch ${options.projectName} for ${options.board} / ${options.part}...`);

        var result = run(options.arduinoBuilder, buildArgs);
        if (result.status != 0)
        {
            process.exit(result.status);
        }
        console.log("");
    }



    // ------------ Upload Phase --------------

    if (upload)
    {
        if (!options.port)
        {
            console.error('Must specify port for upload');
            process.exit(7);
        }

        var hexFile = path.join(options.outputFolder, options.projectName + ".ino.hex");
        var dudeArgs = [
            '-C', options.avrDudeConf,
            '-c', options.protocol,
            '-p', options.part,
            '-P', options.port,
            '-b', options.baud,
            '-D',
            `-Uflash:w:${hexFile}:i`
            ];

        if (options.verbose)
        {
            dudeArgs.push('-v');
            dudeArgs.push('-v');
        }
        else
        {
            dudeArgs.push('-q');
            dudeArgs.push('-q');
        }

        console.log(`Uploading to ${options.part} on ${options.port} at ${options.baud}...`);

        var result = run(options.avrDude, dudeArgs);
        if (result.status != 0)
        {
            process.exit(result.status);
        }
        else
        {
            console.log("Upload completed successfully.");
        }
    }
}

// ------------ Help --------------

function showHelp()
{
    console.log("ArduinoCL - Arduino Command Line Tools");
    console.log("Copyright (C) 2017 Topten Software.  All Rights Reserved");
    console.log();
    console.log("Build Commands:");
    console.log("    acl                       - compile project in current folder");
    console.log("    acl upload                - compile project and upload to board");
    console.log("    acl clean                 - delete build folder");
    console.log("    acl rebuild               - delete build folder and re-compile");
    console.log("    acl clean upload          - rebuild and upload");
    console.log("");       
    console.log("Build Options:");
    console.log("    --port <port>             - port to upload to");
    console.log("    --board <name>            - board name");
    console.log("    --processor <name>        - processor type");
    console.log("    --verbose                 - verbose output");
    console.log("    -P <port>                 - alias for --port");
    console.log("    -v                        - alias for --verbose");
    console.log("");
    console.log("Board Commands:");
    console.log("    acl boards                - list all boards and available processors");
    console.log("");
    console.log("Library Commands:");
    console.log("    acl lib list              - list all known libaries");
    console.log("    acl lib list <string>     - list libaries filtered by string");
    console.log("    acl lib refresh           - refresh the library database from server");
    console.log("    acl lib info <libname> [<version>]");
    console.log("                              - display info about library");
    console.log("    acl lib install <libname> [<version>]");
    console.log("                              - install a library");
    console.log("");
    console.log("Environment Variables: (all optional)");
    console.log("    ARDUINO_DIR               - installation location of the Arduino software");
    console.log("    ARDUINO_PORT              - default port to use for uploads (overrides options, not command line)");
    console.log("");
    console.log("eg: Build project for nano with default processor");
    console.log("      > acl --board nano");
    console.log("");
    console.log("eg: Build project for nano with atmega168 processor");
    console.log("      > acl --board nano --processor atmega168");
    console.log("");
    console.log("eg: Build project for nano with atmega168 processor and upload to COM6");
    console.log("      > acl upload --board nano --processor atmega168 --port COM6");
    console.log("");
    console.log("eg: List all available boards and processors");
    console.log("      > acl boards");
    console.log("");
    console.log("eg: Find libraries with 'neopixel' in name or author");
    console.log("      > acl lib list neopixel");
    console.log("");
    console.log("eg: Install a library");
    console.log("      > acl lib install \"Adafruit NeoPixel\"");
    console.log("");
    console.log("eg: Install a library with a specific version");
    console.log("      > acl lib install \"Adafruit NeoPixel\" \"1.0.2\"");
    console.log("");
    console.log("Instead of having to specify board, processor and port on command line, you use an");
    console.log("options file.");
    console.log("");
    console.log("    ~/ArduinoCL/acl.json      - global options");
    console.log("    ./acl.json                - project options");
    console.log("    ./acl.user.json           - user options (ie: exclude this from version control)");
    console.log("");
    console.log("Options files are merged in the shown order, with later options overriding the same");
    console.log("options from earlier files.  A subkey with the name of the platform (eg: 'win32'");
    console.log("or 'darwin') can be used to specify platform specific options.");
    console.log();
    console.log("For a full list of available options, run acl -v and inspect the resolved option list shown");
    console.log("at the start of output.  A typical option file might contain:");
    console.log("");
    console.log("    {");
    console.log('       "board": "nano",');
    console.log('       "processor": "atmega328",');
    console.log('       "win32":');
    console.log('       {');
    console.log('           "port": "COM5"');
    console.log('       }');
    console.log('       "darwin":');
    console.log('       {');
    console.log('           "port": "/dev/tty.usbmodem"');
    console.log('       }');
    console.log("    }");
}

// ------------ Board Definitions --------------

// Load and parse boards.txt
function parseBoardDefinitions()
{
    var boardInfos = {};
    var defaultProcessors = {};
    var defs = {};
    var lines = fs.readFileSync(options.boardsTxt, 'UTF8').split('\n');
    for (var i=0; i<lines.length; i++)
    {
        var parts = /^(.*)=(.*)$/.exec(lines[i]);
        if (parts)
        {
            var key = parts[1].trim();
            var value = parts[2].trim();
            defs[key] = value;

            parts = key.split('.');
            if (parts.length == 2 && parts[1] == "name")
            {
                boardInfos[parts[0]] = {
                    name: value,
                }
            }
            if (parts.length == 4 && parts[1] == "menu" && parts[2] == "cpu")
            {
                if (boardInfos[parts[0]])
                {
                    if (!boardInfos[parts[0]].processors)
                    {
                        boardInfos[parts[0]].processors = {};
                        defaultProcessors[parts[0]] = parts[3];
                    }

                    boardInfos[parts[0]].processors[parts[3]] = value;
                }
            }
        }
    }

    defs.$boardInfos = boardInfos;
    defs.$defaultProcessors = defaultProcessors;
    return defs;
}


function listBoards()
{
    var boards = parseBoardDefinitions();

    var keys = Object.keys(boards.$boardInfos).sort(function(x,y) {
        return x.localeCompare(y);
    });

    for (var i=0; i<keys.length; i++)
    {
        var bd = boards.$boardInfos[keys[i]];
        console.log(`${keys[i]} = ${bd.name}`);
        if (bd.processors)
        {
            var processors = Object.keys(bd.processors);
            for (var j=0; j<processors.length; j++)
            {
                console.log(`    ${processors[j]} = ${bd.processors[processors[j]]}`);
            }
        }
    }

}
// ------------ Library Tools --------------

// Load the library index, optionally refreshing from server
function* loadLibIndex(refresh)
{
    if (refresh)
    {
        // Delete old library index
        if (fs.existsSync(options.library_index))
            fs.unlinkSync(options.library_index);
    }

    if (!fs.existsSync(options.library_index))
    {
        console.log("Fetching library index...");
        var r = yield requestify.get(options.library_index_url);
        fs.writeFileSync(options.library_index, r.body, 'UTF8');
        return JSON.parse(r.body);
    }
    else
    {
        return JSON.parse(fs.readFileSync(options.library_index, 'UTF8'));
    }
}

// List all libraries, optionally matching filter string
function* listLibraries(filter)
{
    var index = yield* loadLibIndex(false);

    // Get list of libraries
    var libs = index.libraries;

    // Filter by query string
    if (filter)
    {
        filter = filter.toLowerCase();

        libs =libs.filter(function(x) {

            return x.name.toLowerCase().indexOf(filter) >= 0 ||
                    x.author.toLowerCase().indexOf(filter) >= 0;

        });
    }

    // Sort it
    libs = libs.sort(function(x,y) 
    { 
        var cmp = x.name.localeCompare(y.name); 
        if (cmp == 0)
            cmp = -semverCompare(x.version, y.version);
        return cmp;
    });

    // Remove duplicate versions
    for (var i=libs.length-1; i>0; i--)
    {
        if (libs[i].name == libs[i-1].name)
        {
            libs.splice(i, 1);
        }       
    }

    for (var i=0; i<libs.length; i++)
    {
        console.log('"' + libs[i].name + '"', "v"+libs[i].version, "by", libs[i].author);
    }
}

// Show info for a library, optionally matching a version
function* showLibraryInfo(libraryName, version)
{
    // Check asked for a library
    if (!libraryName)
    {
        console.error("Must specify a library name");
        process.exit(7);
    }

    // Load index
    var index = yield* loadLibIndex(false);
    var libs = index.libraries;

    // Filter by library name
    libs =libs.filter(function(x) {
        return x.name.toLowerCase() == libraryName.toLowerCase();
    });

    // Filter by version number
    if (version)
    {
        libs = libs.filter(function(x) { return semverCompare(x.version, version)==0 });
    }

    // Quit if nothing found
    if (libs.length == 0)
    {
        console.error("Unknown library or version:", libraryName, version ? "v" + version : "");
        process.exit(7);
    }

    // Sort by version number descending
    libs = libs.sort(function(x,y) 
    { 
        return -semverCompare(x.version, y.version);
    });

    // Log info using most recent version
    console.log("Name:       ", libs[0].name);
    console.log("Author:     ", libs[0].author);
    console.log("Maintainer: ", libs[0].maintainer);
    console.log("Website:    ", libs[0].website);
    console.log("Category:   ", libs[0].category);
    console.log("Sentence:   ", libs[0].sentence);
    if (version)
        console.log("Version:    ", libs[0].version);
    else
        console.log("Versions:   ", libs.map(x => x.version).join(", "));
    if (libs[0].paragraph)
    {
        console.log();
        console.log(libs[0].paragraph);
    }
}

// Install a library
function* installLibrary(libraryName, version)
{
    // Check asked for a library
    if (!libraryName)
    {
        console.error("Must specify a library name");
        process.exit(7);
    }

    // Load index
    var index = yield* loadLibIndex(false);
    var libs = index.libraries;

    // Filter by library name
    libs =libs.filter(function(x) {
        return x.name.toLowerCase() == libraryName.toLowerCase();
    });

    // Filter by version number
    if (version)
    {
        libs = libs.filter(function(x) { return semverCompare(x.version, version)==0 });
    }

    // Quit if nothing found
    if (libs.length == 0)
    {
        console.error("Unknown library or version:", libraryName, version ? "v" + version : "");
        process.exit(7);
    }

    // Sort by version number descending
    libs = libs.sort(function(x,y) 
    { 
        return -semverCompare(x.version, y.version);
    });

    // Download the file
    var downloadFolder = os.tmpdir();
    var librariesFolder = options.userLibraries;
    var libraryFolderName = libs[0].name.replace(/ /g, '_');
    var libraryFolder = path.join(librariesFolder, libraryFolderName);
    var zipFile = path.join(downloadFolder, path.basename(libs[0].url));
    var extractedFolder = path.join(librariesFolder, path.parse(zipFile).name);

    // Download it
    console.log("Downloading...");
    yield download(libs[0].url, downloadFolder);

    // Remove old folder if it already exists
    if (fs.existsSync(libraryFolder))
    {
        console.log("Removing old library...");
        rmdir(libraryFolder);
    }

    // Extract it to user libraries folder
    console.log("Extracting...", librariesFolder);
    yield Q.nfcall(extractZip, zipFile, {dir: librariesFolder});

    // Rename the new folder without the version number
    fs.renameSync(extractedFolder, libraryFolder);

    // Delete the zip file
    fs.unlinkSync(zipFile);

    // Done!
    console.log("Installed library to", libraryFolder);
}


// ------------ Utility Functions --------------



function merge(x, y)
{
    if (!y)
        return x;

    var keys = Object.keys(y);
    for (var i=0; i<keys.length; i++)
    {
        x[keys[i]] = y[keys[i]];
    }

    return x;
}

function mergeMissing(x, y)
{
    if (!y)
        return x;

    var keys = Object.keys(y);
    for (var i=0; i<keys.length; i++)
    {
        if (!x[keys[i]])
        {
            x[keys[i]] = y[keys[i]];
        }
    }

    return x;
}

function parseOptions(file)
{
    if (!fs.existsSync(file))
        return {};

    try
    {
        var options = JSON.parse(fs.readFileSync(file, 'UTF8'));

        if (options[os.platform()])
        {
            merge(options, options[os.platform()]);
            delete options[os.platform()];
        }

        return options;
    }
    catch (err)
    {
        console.error(`Error parsing options file '${file}' - ${err}`);
        process.exit(7);
    }
}

function escapeArg(x)  
{
    if (os.platform() == "win32")
        return x.indexOf(' ') >= 0 ? `"${x}"` : x;
    else
        return x.replace(/ /g, '\\ ');
}

function run(cmd, args)
{
	if (os.platform() == "win32")
		cmd += ".exe";

    if (options.verbose)
    {
        console.log(cmd, args.map(escapeArg).join(" "));
    }

    return child_process.spawnSync(cmd, args, { stdio: 'inherit' });
}

function getEnv(name, defVal)
{
    if (process.env[name])
        return process.env[name];
    else
        return defVal;
}

function pushOneOrArray(target, arg, value)
{
    if (Array.isArray(value))
    {
        for (var i=0; i<value.length; i++)
        {
            target.push(arg);
            target.push(value[i]);
        }
    }
    else
    {
        target.push(arg);
        target.push(value);
    }

}

function mkdirp(targetDir)
{
    const sep = path.sep;
    const initDir = path.isAbsolute(targetDir) ? sep : '';
    targetDir.split(sep).reduce((parentDir, childDir) => {
      const curDir = path.resolve(parentDir, childDir);
      if (!fs.existsSync(curDir)) {
        fs.mkdirSync(curDir);
      }

      return curDir;
    }, initDir);
}

function rmdir(folder) 
{
    if (fs.existsSync(folder)) 
    {
        fs.readdirSync(folder).forEach(function(file,index)
        {
            var curPath = path.join(folder, file);
            if(fs.lstatSync(curPath).isDirectory()) 
            { 
                rmdir(curPath);
            } 
            else 
            { 
                fs.unlinkSync(curPath);
            }
        });

        fs.rmdirSync(folder);
    }
};